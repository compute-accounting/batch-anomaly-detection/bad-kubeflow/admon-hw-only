import json
import logging
import requests
from typing import *
import kfserving
from itertools import chain


logging.basicConfig(level=kfserving.constants.KFSERVING_LOGLEVEL)


class PreprocessTransformer(kfserving.KFModel):
    def __init__(self, name: str, predictor_host: str):
        super().__init__(name)
        self.model_name = name
        self.predictor_url = predictor_host
        logging.info(f"MODEL NAME {self.name}")
        logging.info(f"PREDICTOR URL {self.predictor_url}")

    def preprocess(self, inputs: List[Dict]) -> Any:
        def unpack_instance(instance: Dict[str,List[Dict[str,float]]]) -> List[List[float]]:
            # foreach instance, get all the values from the dict except for timestamp
            instance_unpacked = []
            for data in instance['data']:
                # dict -> list
                data_vector: List[float] = [data[key] for key in data.keys() if key != "timestamp"]
                # don't want None in my data
                if None in data_vector: continue
                instance_unpacked.append(data_vector)
            return instance_unpacked
            
        logging.info(f"Preprocess-Transformation called with {len(inputs['instances'])} instances")
        preprocessed_data = {"instances": list(chain(*map(unpack_instance, inputs["instances"])))}
        logging.info("Preprocess-Transformation successful, calling model:predict with {len(preprocessed_data['instances'])} datapoints")
        return preprocessed_data

    async def predict(self, request: Dict) -> Dict:
        logging.info(request)
        req_body = json.dumps(request)

        response = requests.post(
            f"http://ml.cern.ch/v1/models/{self.model_name}:predict",
            headers={"Host": f"{self.predictor_url}.svc.cluster.local"},
            data=req_body
        )
        logging.info(response.text)
        return response.json()

